#include "common.h"
#include "mem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

#define ALIGN_V(add, align) (((__intptr_t)(add) + ((align)-1)) & ~((align)-1))

struct glob {
  void *mem;
  size_t taille;
  struct fb *zl;
};

struct fb {
  size_t taille;
  struct fb *next;
};

struct glob *gl;

void mem_init(void *mem, size_t taille) {
  assert(mem == get_memory_adr());
  assert(taille == get_memory_size());
  gl = malloc(sizeof(struct glob));
  gl->mem = mem;
  gl->taille = taille;
  gl->zl = mem;
  gl->zl->taille = taille;
  gl->zl->next = NULL;
  mem_fit(&mem_fit_first);
}

void mem_show(void (*print)(void *, size_t, int)) {
  struct fb *mem = gl->mem;
  struct fb *zl = gl->zl;
  while (mem < (struct fb *)((char *)gl->mem + gl->taille)) {
    if (mem == zl) {
      print(mem, zl->taille, 1);
      mem = (struct fb *)((char *)mem + zl->taille);
      zl = zl->next;
    } else {
      print(mem, mem->taille, 0);
      mem = (struct fb *)((char *)mem + mem->taille);
    }
  }
}

static mem_fit_function_t *mem_fit_fn;
void mem_fit(mem_fit_function_t *f) { mem_fit_fn = f; }

void *mem_alloc(size_t taille) {
  size_t taille_alloue =
      ALIGN_V(sizeof(size_t), ALIGNMENT) + ALIGN_V(taille, ALIGNMENT);
  __attribute__((
      unused)) /* juste pour que gcc compile ce squelette avec -Werror */
  struct fb *fb = mem_fit_fn(gl->zl, taille_alloue);
  if (fb != NULL) {
    struct fb *tmp = gl->zl;
    if (tmp == fb) {
      gl->zl = (struct fb *)((char *)gl->zl + taille_alloue);
      gl->zl->taille = tmp->taille - taille_alloue;
      gl->zl->next = NULL;
    } else {
      while (tmp != NULL) {
        if (tmp->next == fb) {
          struct fb *tmp2 = tmp->next->next;
          tmp->next = (struct fb *)((char *)tmp + taille_alloue);
          tmp->next->next = tmp2;
          tmp->next->taille = tmp->taille - taille_alloue;
          break;
        } else {
          tmp = tmp->next;
        }
      }
    }
    fb->taille = taille_alloue;
    return fb;
  }
  return NULL;
}

void mem_free(void *mem) {
  struct fb *fb_mem = (struct fb *)((char *)mem);
  struct fb *tmp = gl->zl;
  struct fb *parcours_zones = gl->mem;
  int zav = 0; // Indicateur zone avant mem traitée ou non
  struct fb *tmp2;
  // Si on libere la premiere zone
  if (fb_mem <= gl->zl) {
    fb_mem->next = gl->zl;
    gl->zl = fb_mem;
    gl->zl->taille = fb_mem->taille;
    tmp = gl->zl;
    zav = 1;
  }
  while (parcours_zones < (struct fb *)((char *)gl->mem + gl->taille)) {
    if ((struct fb *)((char *)parcours_zones + parcours_zones->taille) ==
        fb_mem) {
      zav = 1; // On indique que la zone avant a ete traitee

      // Si zone libre juste avant mem
      if (parcours_zones == tmp) {
        tmp->taille += fb_mem->taille;
      }
      // Sinon
      else {
        tmp2 = tmp->next;
        tmp->next = mem;
        tmp = tmp->next;
        tmp->taille = fb_mem->taille;
        tmp->next = tmp2;
      }
    }
    // traitement zone apres mem
    if (zav == 1) {
      // Si zone libre apres mem
      if ((struct fb *)((char *)fb_mem + fb_mem->taille) == tmp->next) {
        tmp->taille += tmp->next->taille;
        tmp->next = tmp->next->next;
        break;
      }
    }
    // On avance le pointeur zone libre
    if (parcours_zones == tmp->next) {
      tmp = tmp->next;
    }
    parcours_zones =
        (struct fb *)((char *)parcours_zones + parcours_zones->taille);
  }
}
struct fb *mem_fit_first(struct fb *list, size_t size) {
  struct fb *tmp = list;
  while (tmp != NULL) {
    if (tmp->taille >= size)
      return tmp;
    tmp = tmp->next;
  }
  return NULL;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
  size_t *taille = zone - ALIGN_V(sizeof(size_t), ALIGNMENT);
  return *taille;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb *mem_fit_best(struct fb *list, size_t size) {
  struct fb *min = list;
  struct fb *courant = list;
  while (courant != NULL) { // On parcourt la liste
    if (courant->taille - size > 0 && courant->taille < min->taille)
      min = courant; // si le min trouvé précedemment a une taille superieur à
                     // l'élément courant, on récupère cet élément courant
    courant = courant->next; // On avance dans la liste chainée
  }
  return min;
}

struct fb *mem_fit_worst(struct fb *list, size_t size) {
  struct fb *max = list;
  struct fb *courant = list;
  while (courant != NULL) {
    if (courant->taille - size > 0 &&
        courant->taille >
            max->taille) /*pareil que pour le best sauf qu'on modifie la
                            comparaison pour trouver le max */
      max = courant;
    courant = courant->next;
  }
  return max;
}
